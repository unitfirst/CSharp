﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Task_005_EmployeeList
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Print("Let's start. Select option:");
            Print("1 — display data from file");
            Print("2 — save data to file");

            SelectOption();
        }

        #region Methods

        private static void Print(string text)
        {
            Console.WriteLine($"{text}");
        }

        private static void WrongValue()
        {
            Print("===========================");
            Print("Ooops! Please enter number!");
            Print("===========================");
        }

        private static void CreateFile()
        {
            using (var sw = new StreamWriter("employee.txt", false, Encoding.Unicode))
            {
                var head = string.Empty;
                var title = new[]
                {
                    "ID",
                    "#Added",
                    "#Fullname",
                    "#Age",
                    "#Height",
                    "#Birthday",
                    "#PLace of birth"
                };

                foreach (var item in title) head += item;

                sw.WriteLine(head);
                Print("File created.\n");
            }
        }

        private static string[] OpenFile()
        {
            if (!File.Exists("employee.txt")) CreateFile();

            var lines = File.ReadAllLines("employee.txt", Encoding.UTF8);

            return lines;
        }

        private static void DisplayData()
        {
            if (!File.Exists("employee.txt")) CreateFile();

            using (var sr = new StreamReader("employee.txt", Encoding.UTF8))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var data = line.Split('#');
                    Print($"{data[0],-5}" +
                          $"{data[1],-19}" +
                          $"{data[2],-25}" +
                          $"{data[3],-6}" +
                          $"{data[4],-9}" +
                          $"{data[5],-11}" +
                          $"{data[6],-10}");
                }
            }
        }

        /// <summary>
        ///     Add ID to new data line
        /// </summary>
        /// <param name="lines">Get from OpenFile()</param>
        /// <returns>NEW ID</returns>
        private static int NewId(string[] lines)
        {
            var last = lines.Last().Split(Convert.ToChar("#"));

            if (int.TryParse(last[0], out var newId)) newId += 1;

            Print(newId.ToString());
            return newId;
        }

        /// <summary>
        ///     When input is empty — replace to '-'
        /// </summary>
        /// <param name="inputPhrase">Input</param>
        /// <returns>String '-'</returns>
        private static string ReplaceEmpty(string inputPhrase)
        {
            if (inputPhrase == string.Empty) return "-";

            return inputPhrase;

            // alternative method, but use simple if-else statement, because in other methods and lines there is same "if-else"
            // return string.IsNullOrWhiteSpace(inputPhrase) ? "-" : inputPhrase;
        }

        private static void AppendData(string[] lines)
        {
            using (var sw = new StreamWriter("employee.txt", true, Encoding.Unicode))
            {
                var note = string.Empty;
                var now = DateTime.Now;

                var phrase = new[]
                {
                    "Enter Fullname",
                    "Enter Age",
                    "Enter Height",
                    "Birthday",
                    "PLace of birth"
                };

                note += NewId(OpenFile());
                note += $"#{now.ToShortDateString()} {now.ToShortTimeString()}";

                foreach (var item in phrase)
                {
                    Print($"Enter {item}");
                    note += "#" + ReplaceEmpty(Console.ReadLine());
                }

                sw.WriteLine(note);
                sw.Flush();
            }
        }

        private static void SelectOption()
        {
            while (true)
            {
                var option = Console.ReadLine();

                if (option == "1")
                {
                    Print("You select: display on screen\n");
                    DisplayData();
                    break;
                }

                if (option == "2")
                {
                    Print("Yoy select: add to file\n");
                    AppendData(OpenFile());
                    break;
                }

                if (option == string.Empty)
                {
                    Print("C ya next time!");
                    Environment.Exit(0);
                }

                else
                {
                    WrongValue();
                }
            }
        }
        #endregion
    }
}