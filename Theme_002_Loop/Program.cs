﻿using System;
using System.Threading;

namespace Theme_002_Iteration
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            #region Variables

            var milliseconds = 1000; // Delay for code
            string nextRepeat; // Variable for check valid letter

            int num; // Input number for task 1 & 3

            short hand; // Input amount for start task 2
            short handLimit = 7; // Limit to avoid long game
            string card; // What card player have on hand now
            var sum = 0; // Total of cards on hand
            short i = 1; // Counter for loop

            var isPrime = true;

            #endregion


            #region Methods

            // Message about wrong input value
            void WrongNumber()
            {
                Console.WriteLine("===========================");
                Console.WriteLine("Ooops! Please enter number!");
                Console.WriteLine("===========================");
            }

            // Result in console after delay
            void Calculating()
            {
                Console.Write("\nCalculating");
                Thread.Sleep(milliseconds);
                Console.Write(".");
                Thread.Sleep(milliseconds);
                Console.Write(".");
                Thread.Sleep(milliseconds);
                Console.Write($"\n\nYour number is {num} ");
            }

            #endregion


            #region Task 1

            // Message to input any number
            Console.WriteLine("=========================================================================");
            Console.WriteLine("\nHello! I am a program and I will tell you — is your number even or odd.");
            Console.WriteLine("\n=========================================================================");

            while (true)
            {
                // Check right value input
                while (true)
                {
                    Console.WriteLine("\nEnter any number and press enter key...");

                    if (int.TryParse(Console.ReadLine(), out num)) break;
                    WrongNumber();
                }

                Calculating();

                // Checking numbers
                if (num % 2 == 0)
                    Console.Write("and its EVEN!\n");
                else
                    Console.Write("and its ODD!\n");

                // User need to press Any key for proceed or he can repeat 1st task
                Thread.Sleep(milliseconds);
                Console.WriteLine("\nPress any key to continue...");
                Console.WriteLine("Wanna repeat? Press R...");

                // Check what key user pressed
                nextRepeat = Console.ReadLine().ToLower();
                if (nextRepeat == "r") continue;

                Console.Clear();
                break;
            }

            #endregion


            #region Task 2

            // Message before game will start
            Console.WriteLine("\n=================");
            Console.WriteLine("Let's play Cards!");
            Console.WriteLine("=================");
            Thread.Sleep(milliseconds);


            while (true)
            {
                while (true)
                {
                    Console.WriteLine("\nHow much cards do you have?");

                    // If user enter NOT number — will be error and start again
                    if (!short.TryParse(Console.ReadLine(), out hand))
                    {
                        WrongNumber();
                        continue;
                    }
                    // Ask to user to continue or restart enter less number then limit

                    if (hand > handLimit)
                    {
                        Console.WriteLine("============================================");
                        Console.WriteLine("Are you sure, that you have so much?");
                        Console.WriteLine("\nPress any key to play or Y to try again...");
                        Console.WriteLine("============================================");
                        // Check what key user pressed
                        nextRepeat = Console.ReadLine().ToLower();
                        if (nextRepeat == "y")
                            continue;
                        break;
                    }
                    // It wont be the game with 1 card on hand, because there is no sum in use

                    if (hand < 1)
                    {
                        Console.WriteLine("===============================");
                        Console.WriteLine("Sorry, i dont play with 1 card!");
                        Console.WriteLine("===============================");
                        continue;
                    }

                    break;
                }

                Console.WriteLine("\nNow write a card, and I will add it to the general deck");

                for (i = 1; i < hand; ++i)
                {
                    card = Console.ReadLine()?.ToLower();
                    var attempt = hand - i - 1;

                    #region Methods for loop

                    // Tell in console total amount of cards
                    void TotalAmount()
                    {
                        Console.WriteLine("========================================");
                        Console.WriteLine($"Total amount of your cards: {sum,12}");
                    }

                    // Tell in console — how attempts player left in Cardgame
                    void AttemptCount()
                    {
                        Console.WriteLine($"Cards on hand left: {attempt,20}");
                        Console.WriteLine("========================================");
                    }

                    #endregion

                    AttemptCount();
                    Console.WriteLine("\nEnter your next card...");

                    switch (card)
                    {
                        case "2":
                            sum = sum + 2;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "3":
                            sum = sum + 3;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "4":
                            sum = sum + 4;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "5":
                            sum = sum + 5;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "6":
                            sum = sum + 6;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "7":
                            sum = sum + 7;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "8":
                            sum = sum + 8;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "9":
                            sum = sum + 9;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "10":
                            sum = sum + 10;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "j":
                            sum = sum + 10;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "q":
                            sum = sum + 10;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "k":
                            sum = sum + 10;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        case "t":
                            sum = sum + 10;
                            TotalAmount();
                            AttemptCount();
                            continue;

                        default:
                            Console.WriteLine("======================================================");
                            Console.WriteLine("Nope...Right cards for game: '2 : 10' and 'J, Q, K, T'");
                            Console.WriteLine("======================================================");

                            i -= 1;
                            TotalAmount();
                            AttemptCount();
                            continue;
                    }
                }

                // User need to press Any key for proceed or he can repeat 1st task
                Console.WriteLine("\nGame over!");
                Thread.Sleep(milliseconds);
                Console.WriteLine("\nPress any key to continue...");
                Console.WriteLine("Wanna repeat? Press R...");

                // Check what key user pressed
                nextRepeat = Console.ReadLine().ToLower();
                if (nextRepeat == "r") continue;

                Console.Clear();
                break;
            }

            #endregion


            #region Task 3

            // Message before game will start
            Console.WriteLine("\n==========================================================");
            Console.WriteLine("Final task. Now i will tell you — is your number is prime.");
            Console.WriteLine("==========================================================");

            while (true)
            {
                // Check right value input
                while (true)
                {
                    Console.WriteLine("\nEnter any number and press enter key...");

                    if (int.TryParse(Console.ReadLine(), out num)) break;
                    WrongNumber();
                }

                Calculating();

                i = 2;
                while (i <= num / 2)
                {
                    if (num % i == 0)
                    {
                        isPrime = false;
                        break;
                    }

                    i++;
                }

                if (isPrime)
                    Console.Write("and is a prime!\n");
                else
                    Console.Write("and is not a prime!\n");

                // User need to press Any key for proceed or he can repeat 1st task
                Thread.Sleep(milliseconds);
                Console.WriteLine("\nPress any key to continue...");
                Console.WriteLine("Wanna repeat? Press R...");

                // Check what key user pressed
                nextRepeat = Console.ReadLine().ToLower();
                if (nextRepeat == "r")
                {
                    Console.Clear();
                    continue;
                }

                Console.WriteLine("=========");
                Console.WriteLine("Good bye!");
                Console.WriteLine("=========");
                Console.Clear();
                break;
            }

            #endregion
        }
    }
}