﻿using System;

namespace Task_001_RandomMatrix
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            #region Variables

            int row;
            int col;
            var r = new Random();

            #endregion

            #region Task

            while (true)
            {
                // Check rows
                while (true)
                {
                    Console.WriteLine("\nEnter amount of rows...");

                    if (int.TryParse(Console.ReadLine(), out row)) break;
                    WrongValue();
                }

                // Check cols
                while (true)
                {
                    Console.WriteLine("\nEnter amount of cols...");

                    if (int.TryParse(Console.ReadLine(), out col)) break;
                    WrongValue();
                }

                // Matrix was created
                var arr2d = new int[row, col];
                for (var i = 0; i < row; i++)
                {
                    for (var j = 0; j < col; j++)
                    {
                        arr2d[i, j] = r.Next(10);
                        Console.Write($"{arr2d[i, j],-5} ");
                    }

                    Console.WriteLine();
                }

                // Repeat task?
                if (RepeatTask()) continue;
                break;
            }

            #endregion
        }

        #region Methods

        // Message to repeat
        private static bool RepeatTask()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.WriteLine("Wanna repeat? Press R...");

            // Check what key user pressed
            var yesNo = Console.ReadLine()?.ToLower();

            if (yesNo == "r") return true;
            return false;
        }

        // Message about wrong input value
        private static void WrongValue()
        {
            Console.WriteLine("===========================");
            Console.WriteLine("Ooops! Please enter number!");
            Console.WriteLine("===========================");
        }

        #endregion
    }
}