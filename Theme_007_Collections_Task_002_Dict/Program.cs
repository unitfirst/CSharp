﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Theme_007_Collections_Task_002_Dict
{
    internal class Program
    {
        private static void Open(ref Dictionary<string, string> book)
        {
            var lines = File.ReadAllLines(@"book.csv");
            foreach (var line in lines)
            {
                var tokens = line.Split('\t');
                var key = tokens[0].Trim();
                var value = string.Join("", tokens.Skip(1)).Trim();
                book[key] = value;
            }
        }

        private static void Save(ref Dictionary<string, string> book)
        {
            using (var sw = new StreamWriter(@"book.csv", true, Encoding.Unicode))
            {
                foreach (var kvp in book) sw.WriteLine($"{kvp.Key}\t{kvp.Value}");
            }
        }

        private static void Display(Dictionary<string, string> book)
        {
            Console.WriteLine("\nAccounts: ");
            foreach (var e in book) Console.WriteLine($"{e}");
        }

        private static void NewAccount(ref Dictionary<string, string> book)
        {
            while (true)
            {
                Console.WriteLine("Enter Full Name...");

                var fullName = Console.ReadLine();
                if (fullName == string.Empty)
                {
                    Console.WriteLine("Adding account canceled");
                    break;
                }

                while (true)
                {
                    Console.WriteLine("Enter phone number...");

                    var tel = Console.ReadLine();
                    if (tel == string.Empty)
                    {
                        Console.WriteLine("Adding account canceled");
                        break;
                    }

                    book.Add(tel, fullName);
                    Console.WriteLine($"{tel} added to {fullName}");
                }

                break;
            }

            Save(ref book);
        }

        private static void Find(Dictionary<string, string> book)
        {
            var key = Console.ReadLine();

            if (book.TryGetValue($"{key}", out var value)) Console.WriteLine($"Account: {value}\nNumber: {key}", value);

            Console.WriteLine("Account not found.");
        }

        private static void DeleteAll(ref Dictionary<string, string> book)
        {
            book.Clear();
            Save(ref book);
        }

        public static void Main(string[] args)
        {
            var book = new Dictionary<string, string>();
            Open(ref book);

            Console.WriteLine("\nChoice action:");
            Console.WriteLine("0. Exit program");
            Console.WriteLine("1. Show phonebook");
            Console.WriteLine("2. Add account");
            Console.WriteLine("3. Find account");
            Console.WriteLine("4. Delete all");

            var action = Console.ReadLine();
            switch (action)
            {
                case "0":
                    return;
                case "1":
                    Display(book);
                    break;
                case "2":
                    NewAccount(ref book);
                    break;
                case "3":
                    Find(book);
                    break;
                case "4":
                    DeleteAll(ref book);
                    break;
                default:
                    Console.WriteLine("\nUnknown command. Try again.");
                    break;
            }

            book.Clear();
        }
    }
}