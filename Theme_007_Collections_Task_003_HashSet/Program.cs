﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Theme_007_Collections_Task_003_HashSet
{
    internal class Program
    {
        private static void Add(HashSet<int> hash)
        {
            Console.WriteLine("\nEnter number to add in collection...");

            while (true)
            {
                var input = Console.ReadLine();

                if (int.TryParse(input, out var value))
                {
                    if (!hash.Contains(value))
                    {
                        Console.WriteLine($"{input} added to Hash.");
                        hash.Add(value);
                    }
                    else
                    {
                        Console.WriteLine($"{value} already in Hash.");
                    }
                }
                else
                {
                    if (input == "")
                    {
                        Console.WriteLine("Done.");
                        break;
                    }

                    Console.WriteLine("Unknown type. Try again.");
                }
            }
        }

        private static void Print(HashSet<int> hash)
        {
            Console.WriteLine("Your HashSet:\n");
            foreach (var e in hash) Console.Write($"{e} ");
        }

        public static void Main(string[] args)
        {
            var hashSet = new HashSet<int>();

            Add(hashSet);
            Print(hashSet);
        }
    }
}