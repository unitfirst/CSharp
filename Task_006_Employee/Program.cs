﻿using System;

namespace Task_006_Employee
{
    internal class Program
    {
        private static readonly string Path = "workers.csv";

        public static void Main(string[] args)
        {
            var rep = new Repository(Path);
            while (true)
            {
                Console.WriteLine("\nChoice action:");
                Console.WriteLine("0. Exit program");
                Console.WriteLine("1. Show record");
                Console.WriteLine("2. Create record");
                Console.WriteLine("3. Delete record");
                Console.WriteLine("4. Edit record");
                Console.WriteLine("5. Show records in period");
                Console.WriteLine("6. Show records by descending");
                Console.WriteLine("7. Show records by ascending\n");

                var action = Console.ReadLine();
                switch (action)
                {
                    case "0":
                        return;
                    case "1":
                        rep.ShowRecord();
                        break;
                    case "2":
                        rep.CreateRecord();
                        break;
                    case "3":
                        rep.DeleteRecord();
                        break;
                    case "4":
                        rep.EditRecord();
                        break;
                    case "5":
                        rep.ShowRecordsInPeriod();
                        break;
                    case "6":
                        rep.ShowAscendingRecords();
                        break;
                    case "7":
                        rep.ShowDescendingRecords();
                        break;
                    default:
                        Console.WriteLine("\nUnknown command! Try again.");
                        break;
                }

                rep.Save();
            }
        }
    }
}