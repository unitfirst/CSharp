using System;

namespace Task_006_Employee
{
    public struct Worker
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime AddTime { get; set; }
        public int Height { get; set; }
        public int Age { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }


        public Worker(int id, DateTime addTime, string fullName, int height, int age, DateTime dateOfBirth,
            string placeOfBirth)
        {
            Id = id;
            AddTime = addTime;
            FullName = fullName;
            Height = height;
            Age = age;
            DateOfBirth = dateOfBirth;
            PlaceOfBirth = placeOfBirth;
        }


        /// <summary>
        ///     Take data from line in file, parse and return it by correct type values.
        /// </summary>
        /// <param name="line">Array of data</param>
        /// <returns></returns>
        public static Worker Parse(string line)
        {
            var data = line.Split('#');

            return new Worker(
                int.Parse(data[0]),
                DateTime.Parse(data[1]),
                data[2],
                int.Parse(data[3]),
                int.Parse(data[4]),
                DateTime.Parse(data[5]),
                data[6]
            );
        }

        public override string ToString()
        {
            return $"Id: {Id,-3} " +
                   $"AddTime: {AddTime,-21} " +
                   $"FullName: {FullName,-30} " +
                   $"Age: {Age,-4} " +
                   $"DateOfBirth: {DateOfBirth.ToShortDateString(),-12} " +
                   $"PlaceOfBirth: {PlaceOfBirth,-30}";
        }

        public string Serialize()
        {
            return $"{Id}#{AddTime}#{FullName}#{Height}#{Age}#{DateOfBirth}#{PlaceOfBirth}";
        }
    }
}