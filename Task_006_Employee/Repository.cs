using System;
using System.IO;
using System.Linq;

namespace Task_006_Employee
{
    public class Repository
    {
        private readonly string _path; // Path to file "data.csv"
        private int _lastIndex;
        private Worker[] _workers = new Worker[8]; // Default array size. Increases by 2 when get limit in AddWorker()


        public Repository(string path)
        {
            _path = path;
            File.Open(path, FileMode.OpenOrCreate).Close();
            foreach (var line in File.ReadLines(path)) AddWorker(Worker.Parse(line));
        }


        public void ShowRecord()
        {
            Console.WriteLine("Enter ID for search...\n");
            var id = int.Parse(Console.ReadLine()) - 1;

            if (id >= _lastIndex)
                Console.WriteLine("Record not found.\n");
            else
                Console.WriteLine(_workers[id]);
        }

        public void CreateRecord()
        {
            Console.WriteLine("Enter Full name...\n");
            var fullName = Console.ReadLine();

            Console.WriteLine("Enter height...\n");
            var height = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter age...\n");
            var age = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Date of birth...\n");
            var dateOfBirth = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter Place of birth...\n");
            var placeOfBirth = Console.ReadLine();

            AddWorker(new Worker(
                _lastIndex + 1,
                DateTime.Now,
                fullName,
                height,
                age,
                dateOfBirth,
                placeOfBirth
            ));
        }

        public void DeleteRecord()
        {
            Console.Write("Enter ID for delete...\n");
            var id = int.Parse(Console.ReadLine()) - 1;

            if (id >= _lastIndex) Console.WriteLine("Record not found.\n");

            for (var i = id; i < _workers.Length; i++)
            {
                _workers[i] = _workers[i];
                _workers[i].Id = i;
            }

            _lastIndex--;
        }

        public void EditRecord()
        {
            Console.WriteLine("Enter ID for edit...\n");
            Console.WriteLine("Notice: \n/skip for skip step\n/stop for cancel edit\n");
            var id = int.Parse(Console.ReadLine()) - 1;

            if (id >= _lastIndex) Console.WriteLine("Record not found.");

            Console.WriteLine(_workers[id]);

            Console.WriteLine("Enter NEW full name...\n");
            _workers[id].FullName = Console.ReadLine();
            Console.WriteLine("Enter NEW height...\n");
            _workers[id].Height = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter NEW age...\n");
            _workers[id].Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter NEW date of birth...\n");
            _workers[id].DateOfBirth = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter NEW Place of birth...\n");
            _workers[id].PlaceOfBirth = Console.ReadLine();
        }

        public void ShowRecordsInPeriod()
        {
            Console.WriteLine("Enter start date of period...\n");
            var startTime = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter end date of period...\n");
            var endTime = DateTime.Parse(Console.ReadLine());

            foreach (var worker in _workers.Take(_lastIndex)) Console.WriteLine(worker);
        }

        public void ShowDescendingRecords()
        {
            foreach (var worker in _workers.Take(_lastIndex).OrderByDescending(worker => worker.AddTime))
                Console.WriteLine(worker);
        }

        public void ShowAscendingRecords()
        {
            foreach (var worker in _workers.Take(_lastIndex).OrderBy(worker => worker.AddTime))
                Console.WriteLine(worker);
        }

        public void Save()
        {
            using var sw = new StreamWriter(File.Open(_path, FileMode.OpenOrCreate));
            {
                foreach (var worker in _workers.Take(_lastIndex)) sw.WriteLine(worker.Serialize());
            }
        }

        private void AddWorker(Worker worker)
        {
            if (_lastIndex >= _workers.Length) Array.Resize(ref _workers, _workers.Length * 2);

            _workers[_lastIndex++] = worker;
        }
    }
}