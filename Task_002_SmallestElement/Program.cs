﻿using System;
using static System.Console;

namespace Task_002_SmallestElement
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            #region Variables

            var maxValue = 1000; // This is limit to array (instead of int.Maxlength)
            var minValue = maxValue; // Container for smallest number in array
            int eCount; // Elements count in array

            #endregion


            #region Task

            // Input amount & initialise array
            while (true)
            {
                Write("\nEnter amount of number for array...\t");

                if (int.TryParse(ReadLine(), out eCount)) break;
                WrongValue();
            }

            var array = new int[eCount];

            WriteLine();
            Write($"Available numbers: {-maxValue} & {maxValue}\n\n");

            // Loop for elements in array
            for (var i = 0; i < array.Length; i++)
            {
                Write($"Enter next number, index {i}:\t");

                // Validation input to [int] and [less or more 1000]
                if (int.TryParse(ReadLine(), out array[i]))
                {
                    if (array[i] > maxValue)
                    {
                        LargeValue();
                        i -= 1;
                    }
                    else if (array[i] < -maxValue)
                    {
                        SmallValue();
                        i -= 1;
                    }
                }
                else
                {
                    WrongValue();
                    i -= 1;
                }
            }

            for (var i = 0; i < array.Length; i++)
                if (array[i] < minValue)
                    minValue = array[i];

            Array.Sort(array);

            WriteLine();
            Write("Your array:\t\t\t");

            foreach (var e in array) Write($"{e} ");


            WriteLine("\n");
            WriteLine("===================================");
            WriteLine($"Smalles number in array:\t{minValue}");
            WriteLine("===================================");

            #endregion
        }

        #region Methods

        // Message about wrong input value
        private static void WrongValue()
        {
            WriteLine("===========================");
            WriteLine("Ooops! Please enter number!");
            WriteLine("===========================");
        }

        private static void LargeValue()
        {
            WriteLine("=====================================");
            WriteLine("This number is too large! Enter less.");
            WriteLine("=====================================");
        }

        private static void SmallValue()
        {
            WriteLine("=====================================");
            WriteLine("This number is too small! Enter more.");
            WriteLine("=====================================");
        }

        #endregion
    }
}