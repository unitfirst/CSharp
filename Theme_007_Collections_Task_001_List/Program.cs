﻿using System;
using System.Collections.Generic;

namespace Theme_007_Collections_Task_001_List
{
    internal class Program
    {
        private static void RandomFill(ref List<int> list, int size)
        {
            var rand = new Random();

            for (var i = 0; i < size; i++) list.Add(rand.Next(0, 101));

            Print(list);
        }

        private static List<int> RemoveAtRange(ref List<int> list, int from, int to)
        {
            for (var i = 0; i < list.Count; i++)
                if (list[i] >= from && list[i] <= to)
                {
                    list.RemoveAt(i);
                    i--;
                }

            Print(list);
            return list;
        }

        private static void Print(List<int> list)
        {
            foreach (var i in list) Console.Write($"{i} ");
            Console.WriteLine($"\nList count: {list.Count}");
        }

        public static void Main(string[] args)
        {
            var list = new List<int>();

            RandomFill(ref list, 100);

            RemoveAtRange(ref list, 25, 50);

            list.Clear();
        }
    }
}