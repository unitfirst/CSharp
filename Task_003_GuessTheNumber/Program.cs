﻿using System;

namespace Task_003_GuessTheNumber
{
    internal class Program
    {
        // Message about wrong input value
        private static void WrongValue()
        {
            Console.WriteLine();
            Console.WriteLine("===========================");
            Console.WriteLine("Ooops! Please enter number!");
            Console.WriteLine("===========================");
        }

        // Message about small input value
        private static void SmallValue()
        {
            Console.WriteLine("=====================================");
            Console.WriteLine("This number is too small! Enter more.");
            Console.WriteLine("=====================================");
        }

        public static void Main(string[] args)
        {
            #region Variables

            int setLimit; // Limit to game (example: 999)
            int hiddenNum; // Set hidden number after input the Limit

            var r = new Random();

            #endregion

            Console.WriteLine("Enter an integer greater than 3. I will set a number from [0] to [yours].");

            // Check input
            while (true)
                if (int.TryParse(Console.ReadLine(), out setLimit))
                {
                    if (setLimit < 3)
                    {
                        SmallValue();
                        continue;
                    }

                    break;
                }
                else
                {
                    WrongValue();
                }

            // Set random hidden number
            hiddenNum = r.Next(0, setLimit + 1);
            Console.WriteLine($"{hiddenNum}");

            // Guess the number
            Console.WriteLine($"Now, guess the number between 0 & {setLimit}:\t");

            while (true)
            {
                var input = Console.ReadLine(); // Input value for guess hiddenNum

                if (int.TryParse(input, out var guessNum))
                {
                    if (guessNum < hiddenNum)
                    {
                        Console.WriteLine("\nNumber is bigger. Try again.");
                        continue;
                    }

                    if (guessNum > hiddenNum)
                    {
                        Console.WriteLine("\nNumber is less. Try again.");
                        continue;
                    }
                }
                else
                {
                    if (input == "")
                    {
                        Console.WriteLine($"\nRight number was:\t{hiddenNum}");
                        Console.WriteLine("C ya next time!");
                        break;
                    }

                    WrongValue();
                    continue;
                }

                Console.WriteLine($"\nCongratulations! Right number:\t{hiddenNum}");
                Console.Clear();
                break;
            }
        }
    }
}