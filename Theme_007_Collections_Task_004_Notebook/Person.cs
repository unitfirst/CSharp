using System;
using System.Collections.Generic;
using System.Linq;

namespace Theme_007_Collections_Task_004_Notebook
{
    public class Person
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string FlatNumber { get; set; }
        public string MobilePhone { get; set; }
        public string FlatPhone { get; set; }
        private static List<string> TempList { get; set; }

        public static IEnumerable<Person> GetPerson()
        {
            TempList = EnterRecord();

            return new List<Person>
            {
                new Person
                {
                    Name = TempList.ElementAt(0),
                    Street = TempList.ElementAt(1),
                    HouseNumber = TempList.ElementAt(2),
                    FlatNumber = TempList.ElementAt(3),
                    MobilePhone = TempList.ElementAt(4),
                    FlatPhone = TempList.ElementAt(5)
                }
            };
        }

        private static List<string> EnterRecord()
        {
            TempList = new List<string>();
            var input = new[]
                {"name of person", "street", "house number", "flat number", "mobile phone", "flat phone"};

            foreach (var i in input)
            {
                Console.WriteLine($"Enter {i}...\n");
                TempList.Add(Console.ReadLine());
            }

            return TempList;
        }
    }
}