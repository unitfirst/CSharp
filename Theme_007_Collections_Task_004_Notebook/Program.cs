﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Theme_007_Collections_Task_004_Notebook
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var xmlDoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("Persons",
                    Person.GetPerson().Select(
                        item => new XElement("Person", new XAttribute("name", item.Name),
                            new XElement("Address",
                                new XElement("Street", item.Street),
                                new XElement("HouseNumber", item.HouseNumber),
                                new XElement("FlatNumber", item.FlatNumber)
                            ),
                            new XElement("Phones",
                                new XElement("MobilePhone", item.MobilePhone),
                                new XElement("FlatPhone", item.FlatPhone)
                            )
                        )
                    )
                )
            );

            xmlDoc.Save("persons.xml");
        }
    }
}