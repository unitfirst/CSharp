﻿using System;

namespace Task_004_SeparateText
{
    internal class Program
    {
        private static void Print(string text)
        {
            Console.Write($"{text} ");
        } // Everytime prints on one line

        private static void PrintLine(string text)
        {
            Console.WriteLine($"{text}");
        } // Everytime prints on a new line

        private static string[] ReverseText(string inputPhrase)
        {
            var inverse = SeparateText(inputPhrase);
            Array.Reverse(inverse);

            foreach (var e in inverse) Print(e);

            return inverse;
        } // Rearranges words (back to front)

        private static string[] SeparateText(string inputPhrase)
        {
            var separators = new[] {' ', ',', '.'};
            var arr = inputPhrase.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            return arr;
        } // Every word, separated by " " / "," / ".", will be a new elem in array


        public static void Main(string[] args)
        {
            Console.WriteLine("Enter a long sentence of at least two words");
            var s = Console.ReadLine();

            PrintLine("\nDone.\n");
            ReverseText(s);
        }
    }
}