﻿using System;

namespace Theme_001_Variables
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            /*
                TASK 1. Creating variables and output.
                TASK 2. Implementation of the calculation of the number of points in all subjects.
            */


            // 1 TASK
            // Variables
            var fullName = "Capitan America";
            var eMail = "cap@ameri.ca";
            var age = 29;

            var codeScore = 5.0f;
            var mathScore = 5.0f;
            var physicScore = 4.0f;
            int sumScore;
            float midScore;

            // Display info using pattern
            var pattern =
                "My name: {0} " +
                "\nMy age: {2} " +
                "\nContact me: {1} " +
                "\nMy code score: {3} " +
                "\nMy math score: {4} " +
                "\nMy physics score: {5}";

            Console.WriteLine(
                pattern,
                fullName,
                eMail,
                age,
                codeScore,
                mathScore,
                physicScore);

            // Display info using interpolation (left-allign)
            Console.WriteLine("\n");
            Console.WriteLine($"My name:            {fullName}");
            Console.WriteLine($"My age:             {age}");
            Console.WriteLine($"Contact me:         {eMail}");
            Console.WriteLine($"My code score:      {codeScore}");
            Console.WriteLine($"My math score:      {mathScore}");
            Console.WriteLine($"My physics score:   {physicScore}");
            Console.ReadKey();

            // Display info using interpolation (right align)
            Console.WriteLine("\n");
            Console.WriteLine($"My name: {fullName,20}");
            Console.WriteLine($"My age: {age,21}");
            Console.WriteLine($"Contact me: {eMail,17}");
            Console.WriteLine($"My code score: {codeScore,14}");
            Console.WriteLine($"My math score: {mathScore,14}");
            Console.WriteLine($"My physics score: {physicScore,11}");
            Console.ReadKey();


            // 2 TASK
            sumScore = Convert.ToInt32(codeScore + mathScore + physicScore);
            midScore = (codeScore + mathScore + physicScore) / 3;

            Console.WriteLine($"Total point:        {sumScore}");
            Console.WriteLine("Arithmetic mean:    " + "{0:0.00}",
                midScore); // Format mask for decimals (after dot will be two numbers)
            Console.ReadKey();
        }
    }
}

// Line-1 for test commit